# Changelog


## 15/11/2021

He modificado los archivos Esfera y Raqueta para darles movimiento.
He añadido la funcion Disminuye(float d) a Esfera y lo he puesto en el OnTimer para que disminuya su radio.

## 23/10/2021

He modificado los archivos de cabecera de la clase Mundo y de la clase Esfera mediante dos comentarios
He puesto dos commits, uno diciendo que he modificado la clase Mundo y otro diciendo que he modificado la clase Esfera
